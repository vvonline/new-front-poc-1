module.exports = {
  staticFileGlobs: [
    'dist/**.html',
    'dist/**.js',
    'dist/**.css',
    'dist/assets/images/*',
    'dist/assets/icons/*'
  ],
  root: 'dist',
  stripPrefix: 'dist/',
  navigateFallback: '/index.html',
  runtimeCaching: [{
    urlPattern: /api-b2c\.casasbahia\.com\.br/,
    handler: 'networkFirst'
  },{
    urlPattern: /preco\.api-casasbahia\.com\.br/,
    handler: 'networkFirst'
  },{
    urlPattern: /carrinho\.casasbahia\.com\.br/,
    handler: 'networkFirst'
  },{
    urlPattern: /busca\.api-casasbahia\.com\.br/,
    handler: 'networkFirst'
  }]
};