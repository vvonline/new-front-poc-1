FROM node:boron

# Create app directory
RUN mkdir -p /usr/pwa/dist
WORKDIR /usr/pwa

# Install app dependencies
RUN npm install express

# Bundle app source
COPY server.js /usr/pwa/
COPY dist/. /usr/pwa/dist

EXPOSE 3000
CMD [ "node", "server.js" ]