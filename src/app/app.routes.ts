import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [   
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: 'app/home/home.module#HomeModule' },  
  { path: 'search', loadChildren: 'app/products-search/products-search.module#ProductsSearchModule', data: { search: 'ofertas' } },  
  { path: 'detail', loadChildren: 'app/product-detail/product-detail.module#ProductDetailModule' } 
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes);