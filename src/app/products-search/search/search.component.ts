import { Component, OnInit, Directive } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';

import { ProductApiService } from '../services/product-api.service';
import { Product } from '../models/product';

@Component({
  selector: 'app-feed',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})

export class SearchComponent implements OnInit {
  typeSub: Subscription;
  pageSub: Subscription;
  items: Product[];
  search;
  pageNum: number;
  listStart: number;
  errorMessage: string;

  constructor(
    private _productAPIService: ProductApiService,    
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.router.events.subscribe((evt) => {
        if (!(evt instanceof NavigationEnd)) {
            return;
        }
        window.scrollTo(0, 0)
    });

    this.typeSub = this.route.data.subscribe(data => {
        this.search = (data as any).search;
    });

    this.pageSub = this.route.params.subscribe(params => {
      this.search = params['search'] ? params['search'] : 'tv';
      this._productAPIService.fetchSearch(this.search)
        .subscribe(
          items => this.items = items,          
          error => this.errorMessage = 'Não foi possível pesquisar \"' + this.search + '\"...',
          () => {            
            this.listStart = ((this.pageNum - 1) * 30) + 1;        
          }
        );
    });     
  }
}