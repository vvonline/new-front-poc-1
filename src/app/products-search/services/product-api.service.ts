import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'isomorphic-fetch';
import 'rxjs/add/operator/map';

import { Product } from '../models/product';

declare var fetch;

@Injectable()
export class ProductApiService {
  baseUrl: string;
  apiB2CUrl: string;
  precoApiUrl : string;
  
  constructor() {
    this.baseUrl = 'https://busca.api-casasbahia.com.br';
    this.apiB2CUrl = "https://api-b2c.casasbahia.com.br";
    this.precoApiUrl = "https://preco.api-casasbahia.com.br";
  }
  
  fetchSearch(search: string) : Observable<Product[]> {     
     var productsIds : string = '';
     var normalizedSearchText = search.replace(new RegExp(' ', 'g'), '-');
     var relativeUri = `Produtos/Buscar/?CriterioTiposEntregaEleito=NaoContem&NumPorPagina=20&Ordenacao=&PaginaAtual=1&Palavra=${normalizedSearchText}&TiposEntregaEleito=99`;     

     return new Observable(fetchObserver => {
          let cancelToken = false;

          fetch(`${this.baseUrl}/${relativeUri}`)
              .then((response : any) => {   
                  var products : Array<Product> = [];      
                  
                  response.json().then((searchResult) => {
                     searchResult.IdsProduto.forEach((idProduto : number) => {
                     productsIds += (productsIds === null || productsIds === undefined || productsIds === '') ? idProduto.toString() : `,${idProduto}`;

                     var product = new Product;  
                     product.id = idProduto;
                     products.push(product);                            
                  });                               

                  return products;                      
              }).then((products) => {                  
                  fetch(`${this.apiB2CUrl}/Catalogo/v2/Produtos/Resumo?IdProdutos=${productsIds}&Composicao=Imagens`)
                    .then((response : any) => {
                        response.json().then((resumoResult) => {
                            resumoResult.Produtos.forEach((produto : any) => {
                                  var product = products.filter(p => p.id === produto.IdProduto)[0];                                            
                                  product.rating = produto.Classificacao;
                                  product.name = produto.Nome;

                                  var imagem = produto.Imagem;
                                  product.imageUrl = `https://carrinho.casasbahia.com.br/Control/ArquivoExibir.aspx?IdArquivo=${imagem}`;                                
                            });
                        });                       
                        return products;                                 
                    }).then((products) => {
                        if (!cancelToken) {
                            fetch(`${this.precoApiUrl}/v1/Produtos/PrecoVenda/?IdsProduto=${productsIds}&Composicao=Lojistas,OpcoesParcelamento,DescontoFormaPagamento`)
                              .then((response : any) => {
                                    response.json().then((precoResult) => {
                                            precoResult.PrecoProdutos.forEach((precoProduto : any) => {
                                            var product = products.filter(p => p.id ===  precoProduto.PrecoVenda.IdProduto)[0];
                                            product.priceFrom = precoProduto.PrecoVenda.PrecoDe;
                                            product.priceBy = precoProduto.PrecoVenda.Preco;
                                            product.skuId = precoProduto.PrecoVenda.IdSkuPadrao;                     

                                            var skuId = product.skuId;
                                            product.buyUrl = `http://produto.casasbahia.com.br/${skuId}`;
                                        });      
                                    });
                                    return products;                                                   
                              }).then(products => {
                                fetchObserver.next(products);
                                fetchObserver.complete();
                              });;          
                        }       
                    });               
              });
          });

          return () => {
            cancelToken = true;
          };
     });  
  }
}