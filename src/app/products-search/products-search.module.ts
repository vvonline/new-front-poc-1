import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SearchComponent } from './search/search.component';
import { ItemComponent } from './search-item/item.component';
import { SharedComponentsModule } from '../shared/components/shared-components.module';
import { PipesModule } from '../shared/pipes/pipes.module';

const routes: Routes = [{
    path: ':search',
    component: SearchComponent
}];

@NgModule({
  imports: [ CommonModule, SharedComponentsModule, RouterModule, RouterModule.forChild(routes) , PipesModule ],
  declarations: [ SearchComponent, ItemComponent ],
  exports: [ SearchComponent, RouterModule ],    
})

export class ProductsSearchModule {
}