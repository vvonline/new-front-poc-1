export class Product {
    id: number;
    skuId: number;
    name: string;
    description: string;
    priceFrom: number;
    priceBy: number;
    imageUrl: string;
    buyUrl: string;
    rating: number
}