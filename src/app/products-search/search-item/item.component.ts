import { Component, Input, OnInit } from '@angular/core';
import { Router} from '@angular/router'
import { Product } from '../models/product';

@Component({
  selector: 'item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})
export class ItemComponent implements OnInit {
  @Input() item: Product;  

  constructor(private _route: Router) {  
  }

  onSearch(skuId: number) {
      this._route.navigate([`/detail/${skuId}`]);
  }

  ngOnInit() {}

  get hasUrl(): boolean {    
    return this.item.buyUrl.indexOf('http') === 0;
  }
}