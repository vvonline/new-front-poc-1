import { Component, Input, OnInit, Directive } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute } from '@angular/router';

import { HomeAPIService } from './services/home-api.service';
import { Container } from './models/container';

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],    
})

export class HomeComponent implements OnInit {  
  private errorMessage: string;
  
  private nextPhotoInterval:number = 5000;
  private noLoopSlides:boolean = false;
  private slides:Array<any> = [];
 
  private bannersPage:Array<any> = [];

  constructor(private route: ActivatedRoute
             ,private service: HomeAPIService) {        
  }

  private loadData() {

      this.service.fetch().subscribe(response => {               
          if (response != null) {
            var containers = response.Containers;
            containers.forEach(container => {
              var tipos = container.Tipos.filter(tipo => tipo.Tipo == 1);
              if (tipos.length > 0) {
                var banners = tipos[0].Banners.filter(banner => banner.QuantidadeColunas == 1);
                banners.forEach(banner => {
                    this.slides.push({ 
                      image : `https://carrinho.casasbahia.com.br/Banner/Mobile/${banner.Imagem}.${banner.Extensao}`,
                      url : banner.UrlDestino
                     });
                });
              }

              var typesOutOfCarrousel = container.Tipos.filter(tipo => tipo.Tipo == 2);       
              if (typesOutOfCarrousel.length > 0) {
                  var initialBannersArray : Array<any> = [];

                  var bannersList = typesOutOfCarrousel[0].Banners;//.filter(banner => banner.QuantidadeColunas == 1);
                  bannersList.forEach(banner => {
                    initialBannersArray.push({ 
                        image : `https://carrinho.casasbahia.com.br/Banner/Mobile/${banner.Imagem}.${banner.Extensao}`, 
                        columns : banner.QuantidadeColunas,
                        url : banner.UrlDestino
                      });
                  });

                 var bannerPerRow : any = null;
                 var count : number = 0;

                 initialBannersArray.forEach(banner => {
                    if (banner.columns === 1) {
                      this.bannersPage.push(banner);
                      count = 0;
                      bannerPerRow = null;
                    } else {
                      if (bannerPerRow == null) {
                         count = banner.columns - 1;
                         bannerPerRow = {
                           image : banner.image,
                           columns : banner.columns,
                           url: banner.url,
                           items : []
                         }; 
                         bannerPerRow.items.push({
                           image : banner.image,
                         });
                         this.bannersPage.push(bannerPerRow);
                      } else {
                          if  (count !== 0)
                          { 
                            bannerPerRow.items.push({
                              image : banner.image,
                              url: banner.url,
                            });
                            count--;
                          }

                          if (count == 0)
                            bannerPerRow = null;
                      }
                    }
                 });
              }
            });
          }
      });
  }

  ngOnInit() {               
    this.loadData();
  }
}