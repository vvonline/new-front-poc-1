export class Banner {
    Nome: number;
    Imagem: string;
    Extensao: string;
    UrlDestino: string;
    QuantidadeColunas: number;
}