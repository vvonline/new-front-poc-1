import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SharedComponentsModule } from '../shared/components/shared-components.module';
import { HomeComponent } from './home.component';

const routes: Routes = [{
    path: '',
    component: HomeComponent
}];

@NgModule({  
  imports: [ CommonModule, SharedComponentsModule, RouterModule, RouterModule.forChild(routes) ],
  declarations: [ HomeComponent ],
  exports: [ HomeComponent, RouterModule]  
})

export class HomeModule {

}