import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'isomorphic-fetch';
import 'rxjs/add/operator/map';

import { ContainerResponse } from '../models/container.response'

declare var fetch;

@Injectable()
export class HomeAPIService {
  baseUrl: string;
  
  constructor() {   
  }

  fetch(): Observable<ContainerResponse> {
    return lazyFetch(`https://api-b2c.casasbahia.com.br/Marketing/V3/EstrategiasVenda/Divulgacao/DispositivosMoveis/BannersPublicacao/Mobile?composicao=Container(Container,Tipos,Banners),Tipo(Tipo,Banners),Banner(Nome,UrlDestino,Imagem,Extensao,QuantidadeColunas)&pagina=2&plataforma=0`);
  }  
}

function lazyFetch(url, options?) {
  return new Observable(fetchObserver => {
    let cancelToken = false;
    fetch(url, options)
      .then(res => {
        if (!cancelToken) {
          return res.json()
            .then(data => {
              fetchObserver.next(data);
              fetchObserver.complete();
            });
        }
      }).catch(err => fetchObserver.error(err));
      return () => {
        cancelToken = true;
      };
  });
}

