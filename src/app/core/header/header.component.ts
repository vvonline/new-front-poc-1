import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router'
import { DepartamentoApiService } from './../services/departamento-api.service'
import { Departamento } from '../models/departamento';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {  
  departamentos: Departamento[] = [];  

  constructor(private _route: Router,
              private _service: DepartamentoApiService) {    
  }

  onSearch(value: string) {
     if (value != '' && value != undefined && value != null) {
      this._route.navigate([`/search/${value}`]);
     }
  }

  ngOnInit() {
      this._service.getDepartments().subscribe(response => {                            ;
          response.Departamentos.forEach((departamento:Departamento) => this.departamentos.push(departamento));            
      });
  }  

  scrollTop() {
    window.scrollTo(0, 0);
  }
}
