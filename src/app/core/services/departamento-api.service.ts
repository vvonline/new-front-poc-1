import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Departamento } from '../models/departamento'
import { GetDepartamentoResponse } from '../models/get-departamento-response'
import { Http, Response , RequestOptions, Headers } from '@angular/http'

import 'isomorphic-fetch';
import 'rxjs/add/operator/map';

@Injectable()
export class DepartamentoApiService
{
  baseUrl: string;

  constructor() {
    this.baseUrl = 'https://api-b2c.casasbahia.com.br/Catalogo/v2';
  }

  getDepartments(): Observable<GetDepartamentoResponse> {
      return lazyFetch(`${this.baseUrl}/Categorias/Departamentos/Resumo`);
  }  
}

function lazyFetch(url, options?) {
  return new Observable(fetchObserver => {
    let cancelToken = false;
    fetch(url, options)
      .then(res => {
        if (!cancelToken) {
          return res.json()
            .then(data => {
              fetchObserver.next(data);
              fetchObserver.complete();
            });
        }
      }).catch(err => fetchObserver.error(err));
      return () => {
        cancelToken = true;
      };
  });
}
