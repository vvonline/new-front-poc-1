
import {Component,Input,Output,EventEmitter,OnInit} from '@angular/core'

import {DropdownValue} from './dropdown-value'


@Component({
  selector: 'dropdown',
  template: `
    <div class="dropdown">
      <a>Departamentos</a>
      <div class="dropdown-content">
      <a *ngFor="let value of values" (click)="selectItem(value.label)">{{value.label}}</a>
      </div>
    </div>
  `,
  styles:
  [
     `    
.dropdown {
    position: relative;
    display: inline-block;   
}

.dropdown a {
     color: hsla(0,0%,100%,.8);
    text-decoration: none;
    margin: 0 5px;
    letter-spacing: 1.8px;

    &:hover {
      color: #fff;
    }
}

.dropdown-content {
    display: none;
    position: absolute;
    background-color: #f9f9f9;
    min-width: 250px;
    min-height: 250px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
    overflow: auto;    
    height: 300px; 
}

.dropdown-content a {
    color: black;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
}

.dropdown-content a:hover {background-color: #f1f1f1;cursor:pointer;}

.dropdown:hover .dropdown-content {
    display: block;
}

.dropdown:hover .dropbtn {
    background-color: #3e8e41;
}`
  ]
})
export class DropdownComponent implements OnInit {
  @Input()
  values: DropdownValue[];

  @Output()
  select: EventEmitter<string>;

  ngOnInit() {
  }

  constructor() {
    this.select = new EventEmitter();
  }

  selectItem(value) {
    this.select.emit(value);
  }
}