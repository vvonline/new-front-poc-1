import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { LoaderComponent } from './loader/loader.component';
import { ErrorMessageComponent } from './error-message/error-message.component';
import { Carousel } from './ui/carousel/carousel.component';
import { Slide } from './ui/carousel/slide.component';

@NgModule({
  imports: [CommonModule],
  declarations: [ LoaderComponent, ErrorMessageComponent, Carousel, Slide ],
  exports: [ LoaderComponent, ErrorMessageComponent, Carousel, Slide]
})
export class SharedComponentsModule {}
