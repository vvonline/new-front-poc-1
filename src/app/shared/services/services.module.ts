import { NgModule } from '@angular/core';
import { DepartamentoApiService } from '../../core/services/departamento-api.service';
import { DetalheProdutoApiService } from '../../product-detail/services/detalhe-produto-api.service'
import { ProductApiService } from '../../products-search/services/product-api.service'
import { HomeAPIService } from '../../home/services/home-api.service';

@NgModule({
  imports: [],
  exports : [],
  declarations: [],
  providers: []
})
export class ServicesModule{
  static forRoot() {
    return {
      ngModule: ServicesModule,
      providers: [                
        DepartamentoApiService,
        DetalheProdutoApiService,
        ProductApiService,
        HomeAPIService 
      ]
    }
  }
}

export {    
  DepartamentoApiService,
  DetalheProdutoApiService,
  ProductApiService,
  HomeAPIService 
}