import { NgModule } from '@angular/core';
import { CurrencyFormat } from './currency-format.pipe';

@NgModule({
  declarations: [ CurrencyFormat ],
  exports: [ CurrencyFormat ]
})
export class PipesModule {}
