import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'isomorphic-fetch';
import 'rxjs/add/operator/map';

import { DetalheProdutoResponse } from '../../product-detail/models/DetalheProdutoResponse'
import { ResumoProdutoResponse } from '../../product-detail/models/ResumoProdutoResponse'
import { PrecoResponse } from '../../product-detail/models/PrecoResponse'

declare var fetch;

// wrap fetch in observable so we can keep it chill
@Injectable()
export class DetalheProdutoApiService {
  baseUrl: string;
  
  constructor() {
    this.baseUrl = 'https://api-b2c.casasbahia.com.br/catalogo/v2';
  }

  fetch(sku: number): Observable<DetalheProdutoResponse> {
    return lazyFetch(`${this.baseUrl}/Produto/ObterDetalhes/?Composicao=FichaTecnica,Skus,SkusLojistas,SkusFichaTecnica,SkusServicos,SkuImagens,Categorias,Marcas,RetiraEmLoja&IdSku=${sku}`);
  }

  fetchResumoProduto(sku: number): Observable<ResumoProdutoResponse> {
    return lazyFetch(`${this.baseUrl}/Produtos/Resumo?IdSkus=${sku}&Composicao=Imagens`);
  } 

  fetchPreco(sku: number) : Observable<PrecoResponse> {
    return lazyFetch(`https://preco.api-casasbahia.com.br/v1/skus/${sku}/precovenda`);
  }
}

function lazyFetch(url, options?) {
  return new Observable(fetchObserver => {
    let cancelToken = false;
    fetch(url, options)
      .then(res => {
        if (!cancelToken) {
          return res.json()
            .then(data => {
              fetchObserver.next(data);
              fetchObserver.complete();
            });
        }
      }).catch(err => fetchObserver.error(err));
      return () => {
        cancelToken = true;
      };
  });
}

