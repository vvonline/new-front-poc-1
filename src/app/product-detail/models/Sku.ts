import { SkuLojista } from '../models/SkuLojista';
import { Imagem } from '../models/Imagem';

export class Sku {
    IdSku: number;
    Nome: string;
    NomeCompleto: string;
    OrdemExibicao: number;
    IdArquivoThumbnail: number;
    FlagAtiva: boolean;
    SkuLojistas: SkuLojista[];
    Imagens: Imagem[];
}