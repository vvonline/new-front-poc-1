export class Lojista {
    IdLojista: number;
    Nome: string;
    RetiraEmLojaParceiro: boolean;
    LojistaInternacional: boolean;
}