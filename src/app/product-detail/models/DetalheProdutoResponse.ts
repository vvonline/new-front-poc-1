import { Produto } from '../models/Produto';

export class DetalheProdutoResponse {
    Produto: Produto;
    Valido: boolean;
    Protocolo: string;
}