import { Especificacao } from '../models/Especificacao';

export class FichaTecnica {
    IdCampoValorGrupo: number;
    Nome: string;
    Especificacoes: Especificacao[];
}