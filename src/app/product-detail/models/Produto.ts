import { Imagem } from '../models/Imagem';
import { Sku } from '../models/Sku';
import { FichaTecnica } from '../models/FichaTecnica';
import { Marca } from '../models/Marca';

export class Produto {
    IdProduto: number;
    Nome: string;
    Descricao: string   
    Imagens: Imagem[];
    Canonical: string;
    FlagRetiraEmLoja: boolean;
    FichaTecnica: FichaTecnica[];
    Skus: Sku[];
    Marca: Marca;
}