export class Imagem {
    IdArquivo: number;
    Nome: string;
    Largura: number;
    Altura: number;
    Url: string;
    IdArquivoRelacionado: number
}