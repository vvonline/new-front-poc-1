import { Lojista } from '../models/Lojista';

export class SkuLojista {
    Eleito: boolean;
    Lojista: Lojista;    
    PrecoAnterior: number;
    PrecoVenda: number;
    Fidelizado: false;
}