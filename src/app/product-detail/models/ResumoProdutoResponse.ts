import { ResumoProduto } from '../models/ResumoProduto'

export class ResumoProdutoResponse {
    Produtos: ResumoProduto[]
}