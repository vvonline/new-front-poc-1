export class Marca {
    IdMarca: number;
    Nome: string;
    TextoLink: string;
    Texto: string;
    PalavraChave: string;
    TituloSite: string;
    FlagAtiva: string;
    Contatos: string;
    LinkChat: string;
    Telefone: string;
    EmailSite: string;
    ObservacaoSac: string;
    FlagExibirContato: boolean;
}