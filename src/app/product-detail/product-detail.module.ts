import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { DetailComponent } from './detail/detail.component';
import { SharedComponentsModule } from '../shared/components/shared-components.module';
import { PipesModule } from '../shared/pipes/pipes.module';

const routes: Routes = [{
    path: ':id',
    component: DetailComponent
}];

@NgModule({  
  imports: [ CommonModule, SharedComponentsModule, RouterModule, RouterModule.forChild(routes), PipesModule ],
  declarations: [ DetailComponent ],
  exports: [ DetailComponent, RouterModule ]  
})

export class ProductDetailModule {

}