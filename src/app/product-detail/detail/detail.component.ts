import { Component, Input, OnInit, Directive } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';

import { DetalheProdutoApiService } from '../services/detalhe-produto-api.service';
import { DetalheProdutoResponse } from '../models/DetalheProdutoResponse';

import { Produto } from '../models/Produto';
import { FichaTecnica } from '../models/FichaTecnica';

@Component({
  selector: 'product-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss'],    
})

export class DetailComponent implements OnInit {  
  private pageSub: Subscription;  
  private errorMessage: string;

  private skuId : number;
  private productName: string;
  private productUrl: string;
  private rating: number;
  private description: string;
  private ficha: any;
  private brandId: number;
  private brand: string;
  private precoDe : number;
  private precoPor: number;
    
  //The time to show the next photo
  private nextPhotoInterval:number = 5000;
  //Looping or not
  private noLoopSlides:boolean = false;
  //Photos
  private slides:Array<any> = [];

  constructor(private route: ActivatedRoute
             ,private router: Router
             ,private service: DetalheProdutoApiService) {        
  }

  private loadData() {
    this.pageSub = this.route.params.subscribe(params => {
      this.skuId = params['id'] ? params['id'] : 0;        
      this.service.fetch(this.skuId).subscribe(detalheProdutoResponse => {               
        if (detalheProdutoResponse != null && detalheProdutoResponse != undefined && detalheProdutoResponse.Produto != null && detalheProdutoResponse.Produto != undefined) {
          this.productName = detalheProdutoResponse.Produto.Nome;   
          this.description = detalheProdutoResponse.Produto.Descricao.replace(/(?:\r\n|\r|\n)/g, '<br />');    
          this.brandId = detalheProdutoResponse.Produto.Marca.IdMarca;
          this.brand = detalheProdutoResponse.Produto.Marca.Nome;

          this.service.fetchPreco(this.skuId).subscribe(response => {
              if (response != null) {                
                this.precoDe = response.PrecoVenda.PrecoDe;
                this.precoPor  = response.PrecoVenda.Preco;                
              }
          });          
          
          if (detalheProdutoResponse.Produto.FichaTecnica != null && detalheProdutoResponse.Produto.FichaTecnica != undefined) {
            detalheProdutoResponse.Produto.FichaTecnica.forEach(ficha => {
                ficha.Especificacoes.forEach(especificacao => {                                                        
                     especificacao.Valor = especificacao.Valor.replace(/(?:\r\n|\r|\n)/g, '<br />');              
                });
            });
            this.ficha = detalheProdutoResponse.Produto.FichaTecnica;             
          }
          
          if (detalheProdutoResponse.Produto.Skus.length > 0 && detalheProdutoResponse.Produto.Skus[0].Imagens.length > 0) {                  
            var imagens = detalheProdutoResponse.Produto.Skus[0].Imagens.filter(imagem => imagem.Largura == 500);
            imagens.forEach(img => {
              this.slides.push({ image : img.Url });
            });                        
            this.service.fetchResumoProduto(this.skuId).subscribe(resumoProdutoResponse => {
              if (resumoProdutoResponse != null && resumoProdutoResponse.Produtos != null && resumoProdutoResponse.Produtos.length > 0) {      
                this.rating = resumoProdutoResponse.Produtos[0].Classificacao;
                this.productUrl = `https://carrinho.casasbahia.com.br/Control/ArquivoExibir.aspx?IdArquivo=${resumoProdutoResponse.Produtos[0].Imagem}`;          
              }
            });          
          }        
        }
      });            
    });
  }

  onWeddingClick() : void {
      throw "Invalid button click!";
  }

  ngOnInit() { 
     this.router.events.subscribe((evt) => {
        if (!(evt instanceof NavigationEnd)) {
            return;
        }
        window.scrollTo(0, 0)
    });
                  
    this.loadData();
  }
}