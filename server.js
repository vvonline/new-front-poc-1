var express = require('express')
var app = express()
var path = __dirname + '/dist';

app.use(express.static(path));
app.get('*', function(req, res) {
    res.sendFile(path + '/index.html');
});
var port = process.env.PORT || 3000; 	
app.listen(port);