## POC PWA - A PROGRESSIVE CLIENT BUILT WITH ANGULAR2

:zap: **Fast:** Service Worker App Shell + Dynamic Content model to achieve faster load times with and without a network.

:iphone: **Responsive:** Completely responsive UI that can be installed to your mobile home screen to provide a native feel.

:rocket: **Progressive:** [Lighthouse](https://github.com/GoogleChrome/lighthouse) score of 99/100.

## Offline Support

This app uses a Service Worker to load quickly and work offline.

* [`sw-precache`](https://github.com/GoogleChrome/sw-precache) is used to serve local static resources (App Shell) cache first.
* [`sw-toolbox`](https://github.com/GoogleChrome/sw-toolbox) is used to handle requests using the `networkFirst` strategy. If a request fails, the app will fulfill the request from the cache. This means previously loaded pages will now work offline.

## Manifest

With Chromium based browsers for Android (Chrome, Opera, etc...), PWA POC includes a Web App Manifest that allows you to install to your homescreen.

## Possible areas of improvement

 - Realtime updating
 - Server side rendering (Angular Universal?)

## Build process

 - Clone or download the repo
 - If you don't have Angular CLI installed: `npm install -g angular-cli@latest`
 - `ng init`
 - Input `n` for each file to not overwrite any file changes
 - `ng serve` will kick off the server at `http://localhost:4200/`. Any changes you do to the source files should automatically reload the app
 - `ng serve --prod --aot` will kick off a production build with uglifying, tree-shaking and Ahead-of-Time compilation

Click [here](https://cli.angular.io/) to see a full list of what you can do with Angular CLI.

Note: Any Service Worker changes will not be reflected when you run the application locally in development. To test service worker changes:
 - `ng build --prod --aot` to kick off a fresh build and update the `dist/` directory
 - `npm run precache` to generate the service worker file
 - `npm run static-serve` to load the application along with the service worker asset using [live-server](https://github.com/tapio/live-server)

## CORS

- To prevent CORS errors, like `No 'Access-Control-Allow-Origin' header is present on the requested resource`, use [this](http://stackoverflow.com/questions/3102819/disable-same-origin-policy-in-chrome) aproach on [Chrome](http://stackoverflow.com/questions/3102819/disable-same-origin-policy-in-chrome).
